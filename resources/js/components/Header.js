import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from "react-router-dom";
import { gsap } from "gsap";

export default class Header extends Component {

	render() {
            return (

            	<div>

            		<div className="toggle-btn">
            			<span className="one"></span>
            			<span className="two"></span>
            		</div>

            		<div className="menu">
            			<div className="data">
            				<ul>
            					<li>Navigation</li>
            					<li><Link to="#"> About</Link></li>
            					<li><Link to="#"> Projects</Link></li>
            					<li><Link to="#"> Contact</Link></li>
            				</ul>
            			</div>
            		</div>
                        	
                  </div>
            
            );
      }
}
